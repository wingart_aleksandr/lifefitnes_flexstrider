;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;
			
			self.mainMenu();
			self.sliderHeight.init();
			self.tooltip.init();
			self.videoPlaySlider.init();
			self.InputFieldsCheck();

		},

		windowLoad: function(){

			var self = this;
			
			self.videoPlay();
			self.swiperNav.init();
			
		},

		/**
		**	Video
		**/

		videoPlay:function(){
			
			if($('html').hasClass('md_no-touch')){

				if($("#my-video").length){

					$("#my-video")[0].play();
					
				}

			}
		
		},

		/**
		**	Main Menu
		**/

		mainMenu: function(){

			$('.menu_btn').on('click', function(){
				$(this).toggleClass('active');
				$('.main_menu_box').toggleClass('open');
			});

		},

		/**
		**	Slider 
		**/

		sliderHeight: {

			init: function(){
				
				var self = this;

				self.w 			   = $(window);
				self.container     = $('.swiper-container');
				self.slider 	   = self.container.find('.swiper-slide');
				self.sliderNav 	   = $('.dg-navigation');

				if(self.w.width() >= 768){

					self.calculateHeight();

					setTimeout(function(){
						self.calculateHeight();
					},1000)
					
					self.w.on('resize', function(){
						setTimeout(function(){
							self.calculateHeight();
						},1000)
					})
					
				}

			},

			calculateHeight: function(){
				
				var self = this;

				self.sliderH 	    = self.container.height();
				self.sliderW 	    = self.slider.width();
				
				self.navW = (self.w.width() - self.sliderW) / 2;

				self.sliderNav.find('div').height(self.sliderH).width(self.navW);	

			},

		},

		/**
		**	Tooltip 
		**/

		tooltip: {
			
			init: function(){
				
				var self = this;

				self.html = $('html');
				self.tooltipBox = $('.tooltip_box');
				self.tooltipBtn = $('.tooltip_btn');
				self.tooltipContent = $('.tooltip_content');

				self.checkTouchDevice();

			},

			checkTouchDevice: function(){

				var self = this;

				if(self.html.hasClass('md_touch')){

					self.tooltipBox.on('click', function(){
						
						if(!$(this).hasClass('active')){
							$(this).removeClass('no_active').addClass('active');
						}

						else if($(this).hasClass('active')){
							$(this).removeClass('active').addClass('no_active');
						}

					});

				}
				else{

					self.tooltipBox.hover(function(){
						$(this).removeClass('no_active').addClass('active');
					}, function(){
						$(this).removeClass('active').addClass('no_active');
					});

				}

			},

		},

		/**
		**	Play video
		**/

		videoPlaySlider: {
			init: function(){

				var self = this;

				self.sliderSection = $('.slide');
				self.sectionConsole = $('.section_console2');
				self.sectionConsole2 = $('.section_slider');
				self.touchSlide = $('.touch_slide');
				self.respTabContent = $('.resp-tab-content');
				self.videoBox = $('.slider_video');
				self.videoIcon = $('.video_icon_play');
				self.sliderImg = $('.slider_item_img');

				self.playVideo();

			},

			playVideo: function(){

				var self = this;


				self.videoIcon.on('click', function(){
					
					self.videoSrc = $(this).attr('data-src');
					self.videoFrame = $('.video_frame');

					if($(this).closest(self.sliderSection).hasClass('active')){
						$(this).hide();
						$(this).closest(self.videoBox).addClass('active').find(self.sliderImg).hide();
						$(this).closest(self.sliderSection).find(self.videoFrame).attr('src',self.videoSrc+'?rel=0&amp;wmode=transparent&amp;autoplay=true');
					}

					if($(this).closest(self.sectionConsole).hasClass('active')){
						if($(this).closest(self.respTabContent).hasClass('resp-tab-content-active')){
							$(this).hide();
							$(this).closest(self.videoBox).addClass('active').find(self.sliderImg).hide();
							$(this).closest(self.respTabContent).find(self.videoFrame).attr('src',self.videoSrc+'?rel=0&amp;wmode=transparent&amp;autoplay=true');
						}
					}

					if($(this).closest(self.sectionConsole2).hasClass('active')){
						$(this).hide();
						$(this).closest(self.videoBox).addClass('active').find(self.sliderImg).hide();
						$(this).closest(self.touchSlide).find(self.videoFrame).attr('src',self.videoSrc+'?rel=0&amp;wmode=transparent&amp;autoplay=true');
					}

				});

			},

		},

		/**
		**	Input Fields Check
		**/

		InputFieldsCheck: function(){

			var self = this;

			$('input').on('click', function(){

				var $this = $(this),
					formBox = $('.form_row'),
					value = $this.val();

				if($this.is(':focus')){
					$this.addClass('focus');	
					$this.closest(formBox).addClass('focus_row');
				}
				else{
					$this.removeClass('focus');
					$this.closest(formBox).removeClass('focus_row');
				}

			});

			$('input').on('blur', function(){

				var $this = $(this),
					formBox = $('.form_row'),
					value = $this.val();

				if(value == ''){
					$this.removeClass('focus');
					$this.closest(formBox).removeClass('focus_row');
				}
				else{
					$this.addClass('focus');	
					$this.closest(formBox).addClass('focus_row');
				}

			});

		},

		/**
		**	SwiperNav
		**/

		swiperNav: {

			init: function(){

				var self = this;

				self.consoleLink = $('.console_navigation a');
				self.consoleClick = $('.console_click a');
				self.consoleTabLink = $('.resp-tabs-list a');

				self.initialisePlugin();
				self.method();
				self.callback();

			},

			initialisePlugin: function(){

				var self = this;
					
				if($('.swiper-container').length){
					self.swiper = new Swiper('.swiper-container', {
				        pagination: '.swiper-pagination',
	        			nextButton: '.dg-next',
	        			prevButton: '.dg-prev',
				        slidesPerView: 3,
				        paginationClickable: true,
				        centeredSlides: true,
				        loopedSlides: 3,
				        // loop: true,
				        breakpoints: {
						    767: {
						      	slidesPerView: 1,
								loopedSlides: 1
						    }
						}
				    });
				}

			},

			method: function(){

				var self = this;

				var indexConsole = self.consoleClick.parent('.active').index();
				
				self.swiper.slideTo(indexConsole);

				self.tabsIndex(indexConsole);
 
				self.consoleLink.on('click', function(){

					var indexLink = $(this).parent().index();
						

					self.swiper.slideTo(indexLink);

					self.tabsIndex(indexLink);

				});

				$('.resp-accordion').live('click', function(){

					var indexLink = $(this).index('.resp-accordion');

					self.swiper.slideTo(indexLink);

					self.consoleLink.parent().eq(indexLink).addClass('active').siblings().removeClass('active');

				});
				
			},

			tabsIndex: function(index){

				var self = this;

				$('.resp-tab-content[aria-labelledby="tab_item-'+ index +'"]').show().addClass('resp-tab-content-active').siblings('.resp-tab-content').hide().removeClass('resp-tab-content-active');

				$('.resp-accordion[aria-controls="tab_item-'+ index +'"]').addClass('resp-tab-active').siblings('.resp-accordion').removeClass('resp-tab-active');
				
				$('.resp-tab-item[aria-controls="tab_item-'+ index +'"]').addClass('resp-tab-active').siblings('.resp-tab-item').removeClass('resp-tab-active');

			},

			callback: function(){

				var self = this;

				self.swiper.on('slideChangeEnd', function () {
				    
				    self.consoleLink.parent().eq(self.swiper.activeIndex).addClass('active').siblings().removeClass('active');

				    self.tabsIndex(self.swiper.activeIndex);

				});
			},

		},


	}


	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).load(function(){

		Core.windowLoad();

	});

})(jQuery);