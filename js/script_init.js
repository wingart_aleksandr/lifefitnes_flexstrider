//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/

;(function($){

	"use strict";


		var pageSlider   = $('#page_slider'),
			validate     = $('.validate'),
			mask	     = $('input[type="tel"]'),
			animation    = $('.wow'),
			slider       = $('.swiper-container'),
			touchslider1 = $('.touch_gallery'),
			touchslider2 = $('.touch_gallery2'),
			tooltip      = $('.tooltip_btn'),
			tabs         = $('.tabs');

		$(document).ready(function(){



			/* ------------------------------------------------
			Tabs START
			------------------------------------------------ */

			function tab(){
				if($('.tabs').length){
					$('.tabs').easyResponsiveTabs({
						activate: function(event) {
							
							if($('.section_console2').hasClass('active')){
								function tabsRemoveVideo(){
									$('.resp-tab-content').find($('.video_icon_play')).show();
									$('.resp-tab-content').find('.slider_video').removeClass('active').find($('.slider_item_img')).show();
									$('.resp-tab-content').find('.video_frame').attr('src','');
								}
								tabsRemoveVideo();
							}

						}
					});
				}
			}
			tab();

			/* ------------------------------------------------
			Tabs END
			------------------------------------------------ */



			/* ------------------------------------------------
			Page slider START
			------------------------------------------------ */
			
			if(pageSlider.length){
		          	
				pageSlider.fullpage({
					scrollHorizontally: true,
					menu: '#page_navigation',
					controlArrows: false,
					slidesNavigation: true,
					recordHistory: false,
					anchors:['slide1', 'slide2', 'slide3' , 'slide3_5' , 'slide4' , 'slide5'],
					responsiveWidth: 1024,
					disableMouse: true,
					onLeave: function(index, nextIndex, direction){

						if(nextIndex == 1 && $("#my-video").length){
							if($('html').hasClass('md_no-touch')){

								$("#my-video")[0].play();
									
							}
						}

		               	var bodyClass = $($('.section')[nextIndex-1]).attr('data-label');

		               	$('body').attr('data-label', bodyClass);

		               	if(direction == 'up' || direction == 'down'){

							$('.video_icon_play').show();
							$('.video_icon_play').closest('.slide, .section_console2, .touch_slide').find('.slider_video').removeClass('active');
							$('.video_icon_play').closest('.slide, .section_console2, .touch_slide').find('.slider_item_img').show();
							$('.video_icon_play').closest('.slide, .section_console2, .touch_slide').find('.video_frame').attr('src','');

							function offset(){
								if(!$('.section_console').hasClass('active')){
									$('.section_console').addClass('offsetAnimate');
								}
								else if($('.section_console').hasClass('active')){
									$('.section_console').removeClass('offsetAnimate');
								}
							}
							offset();

						}

						$('.console_click').find('li[data-index="'+nextIndex+'"]').addClass('active').siblings().removeClass('active');

					},
					afterLoad: function(anchorLink, index){
						
		               	var bodyClass = $($('.section')[index-1]).attr('data-label');

		               	$('body').attr('data-label', bodyClass);
		               	
					},
					onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){

						var item = $('[data-anchor="'+anchorLink+'"]').find('.slide').eq(slideIndex);

						if(item.find('.slider_video').length){

							$('.video_icon_play').show();
							$('.video_icon_play').closest('.slide, .touch_slide').find('.slider_video').removeClass('active');
							$('.video_icon_play').closest('.slide, .touch_slide').find('.slider_item_img').show();
							$('.video_icon_play').closest('.slide, .touch_slide').find('.video_frame').attr('src','');

						}

					}

	            });

    			$('.console_click').on('click', 'li[data-index]', function(){

	            	var index = $(this).attr('data-index');
	            	setTimeout(function(){
						pageSlider.fullpage.moveTo(index);
	            	},800)

	            });
            
			}

			/* ------------------------------------------------
			Page slider END
			------------------------------------------------ */



			/* ------------------------------------------------
			Inputmask START
			------------------------------------------------ */

			if(mask.length){

				mask.inputmask({
					"mask": "+7 (999) 999-9999",
					'showMaskOnHover': false,
					"clearIncomplete": true,
					'oncomplete': function(){ 
						// console.log('Ввод завершен'); 
					}, 
					'onincomplete': function(){ 
						// console.log('Заполнено не до конца'); 
					},
					'oncleared': function(){ 
						// console.log('Очищено'); 
					}
				});

			}

			/* ------------------------------------------------
			Inputmask END
			------------------------------------------------ */


			/* ------------------------------------------------
			Validate START
			------------------------------------------------ */

			if(validate.length){

				validate.validate({

					rules:{

						cf_name: {
							required: true,
							minlength: 2
						},

						cf_email: {
							required: true,
							email: true
						},

						cf_tel: {
							required: true
						},

					},

					messages:{

						cf_name: {
							required: "Поле обязательно для заполнения",
							minlength: 'Введите не менее 2 символов.'
						},

						cf_email: {
							required: "Поле обязательно для заполнения",
							email: "Не верный email."
						},

						cf_tel: {
							required: "Поле обязательно для заполнения",
							inlength: "Введите не менее 10 символов."
						},

					}

				});	

			}

			/* ------------------------------------------------
			Validate END
			------------------------------------------------ */			
			
		});


		$(window).load(function(){

			/* ------------------------------------------------
			ANIMATION START
			------------------------------------------------ */

			if(animation.length){

				// if($('html').hasClass('md_no-touch')){
					animation = new WOW({
					    boxClass: 'wow',
					    animateClass: 'animated'
					});
					animation.init();
				// }
				
			}

			/* ------------------------------------------------
			ANIMATION END
			------------------------------------------------ */


			/* ------------------------------------------------
			Touch swipe END
			------------------------------------------------ */

			if($('html').hasClass('md_touch')){

				if(touchslider1.length){
				    if($(window).width() <= 767){
						var swiper = new Swiper(touchslider1, {});
					    swiper.destroy();
				    }
				    else if($(window).width() >= 768){
						var swiper = new Swiper(touchslider1, {
					        pagination: '.swiper-pagination',
					        slidesPerView: 1,
					        centeredSlides: true,
					        loop: true,
					        breakpoints: {
							    767: {
							      	slidesPerView: 1,
									loopedSlides: 1
							    }
							}
					    });
				    }
				}
				if(touchslider2.length){
					var swiper2 = new Swiper(touchslider2, {
				        pagination: '.swiper-pagination',
	        			slideToClickedSlide: true,
				        slidesPerView: 1,
				        centeredSlides: true,
				        loop: true,
				        breakpoints: {
						    767: {
						      	slidesPerView: 1,
								loopedSlides: 1
						    }
						},
						onSlideChangeStart: function (swiper) {
		                    $('.video_icon_play').show();
							$('.video_icon_play').closest('.slide, .section_console2, .touch_slide').find('.slider_video').removeClass('active');
							$('.video_icon_play').closest('.slide, .section_console2, .touch_slide').find('.slider_item_img').show();
							$('.video_icon_play').closest('.slide, .section_console2, .touch_slide').find('.video_frame').attr('src','');
		                },
				    });
				}

			}

			/* ------------------------------------------------
			Touch swipe END
			------------------------------------------------ */

		});



})(jQuery);


//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
